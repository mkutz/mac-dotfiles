" This is Gary Bernhardt's .vimrc file
" vim:set ts=2 sts=2 sw=2 expandtab:

autocmd!
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" BASIC EDITING CONFIGURATION
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
" allow unsaved background buffers and remember marks/undo for them
set hidden
" remember more commands and search history
set history=10000
set tabstop=2
set shiftwidth=2
set softtabstop=2
set et
set autoindent
set smartindent
set laststatus=2
set showmatch
set incsearch
set hlsearch
" make searches case-sensitive only if they contain upper-case characters
set ignorecase smartcase
set cursorline cursorcolumn
" highlight current line
hi CursorLine cterm=bold ctermbg=16 guibg=DarkGrey
set cmdheight=1
set switchbuf=useopen
set showtabline=2
set winwidth=79
" Prevent Vim from clobbering the scrollback buffer. See
" http://www.shallowsky.com/linux/noaltscreen.html
set t_ti= t_te=
" keep more context when scrolling off the end of a buffer
set scrolloff=5
" Don't make backups at all
set nobackup
set nowritebackup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
" allow backspacing over everything in insert mode
set backspace=indent,eol,start
" display incomplete commands
set showcmd
" Enable highlighting for syntax
syntax on
" use emacs-style tab completion when selecting files, etc
set wildmode=longest,list
" make tab completion for files/buffers act like bash
set wildmenu
let mapleader="\\"
" Fix slow O inserts
:set timeout timeoutlen=1000 ttimeoutlen=100
" Normally, Vim messes with iskeyword when you open a shell file. This can
" leak out, polluting other file types even after a 'set ft=' change. This
" variable prevents the iskeyword change so it can't hurt anyone.
let g:sh_noisk=1
" Modelines (comments that set vim options on a per-file basis)
set modeline
set modelines=3
" Turn folding off for real, hopefully
set foldmethod=manual
set nofoldenable
" Insert only one space when joining lines that contain sentence-terminating
" punctuation like `.`.
set nojoinspaces
" If a file is changed outside of vim, automatically reload it without asking
set autoread
set relativenumber number
set visualbell
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" CUSTOM AUTOCMDS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup vimrcEx
  " Clear all autocmds in the group
  autocmd!
  autocmd FileType text setlocal textwidth=78
  " Jump to last cursor position unless it's invalid or in an event handler
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

  autocmd! BufNewFile,BufRead *.md setlocal ft=markdown
  autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:&gt;
  autocmd BufRead *.markdown  set ai formatoptions=tcroqn2 comments=n:&gt;


  " Don't syntax highlight markdown because it's often wrong
  " autocmd! FileType mkd setlocal syn=off

  " indent slim two spaces, not four
  autocmd! FileType *.slim set sw=2 sts=2 et

  " in location window q is close
  autocmd FileType qf nnoremap <buffer> q :q<cr>
  
  " Lua
  autocmd FileType lua nnoremap  <buffer> <cr> :call CocAction("diagnosticToggle")<cr>
  autocmd FileType lua nnoremap  <buffer> <silent>  <leader>rl :silent !make code-reload<cr>
  autocmd FileType lua nnoremap  <buffer> <silent>  <leader>tl :silent !make test-code-reload<cr>
  autocmd BufWrite *.lua :CocDiagnostics 3
  autocmd BufWrite *.lua :lw

  " Openresty
  autocmd BufRead,BufNewFile *.conf set filetype=nginx
  autocmd BufRead,BufNewFile *.conf set expandtab shiftwidth=4 softtabstop=4 tabstop=4


  " Prettier
  autocmd FileType js,javascript,typescript,ts nnoremap  <buffer> <cr> :PrettierAsync<cr>:w<cr>
  autocmd FileType js,javascript,typescript,ts vnoremap  <buffer> <cr> :PrettierPartial<cr>:w<cr>
augroup END


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" STATUS LINE
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MISC KEY MAPS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <leader>y "+y
map <leader>p "+p
map <leader>P "+P
" Move around splits with <c-hjkl>
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l
" Insert a hash rocket with <c-l>
imap <c-l> <space>=><space>
" Can't be bothered to understand ESC vs <c-c> in insert mode
inoremap <c-c> <esc>
nnoremap <leader><leader> <c-^>
inoremap jk <esc>
map <c-q> :noh<cr>
map <leader>dd :bd<cr>
map <leader>DD :bd!<cr>
map <c-a> :bp<cr>
map <c-e> :bn<cr>
map ; :
map <leader>t :NERDTreeToggle<cr>
map <leader>g :NERDTreeFind<cr>
map <leader>rc :e ~/.nvimrc<cr>
map <leader>q :noh<cr>
map j gj
map k gk

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MULTIPURPOSE TAB KEY
" Indent if we're at the beginning of a line. Else, do completion.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" function! InsertTabWrapper()
"     let col = col('.') - 1
"     if !col || getline('.')[col - 1] !~ '\k'
"         return "\<tab>"
"     else
"         return "\<c-n>"
"     endif
" endfunction
" inoremap <expr> <tab> InsertTabWrapper()
" inoremap <s-tab> <c-n>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" RENAME CURRENT FILE
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! RenameFile()
    let old_name = expand('%')
    let new_name = input('New file name: ', expand('%'), 'file')
    if new_name != '' && new_name != old_name
        exec ':saveas ' . new_name
        exec ':silent !rm ' . old_name
        redraw!
    endif
endfunction
map <leader>n :call RenameFile()<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype indent off

call plug#begin('~/.config/nvim/plugged')

Plug 'VundleVim/Vundle.vim'
Plug 'tpope/vim-commentary'
Plug 'Raimondi/delimitMate'
Plug 'tpope/vim-repeat'
Plug 'pangloss/vim-javascript'
Plug 'bling/vim-airline'
Plug 'tpope/vim-surround'
Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'chriskempson/base16-vim'
Plug 'spacewander/openresty-vim'
Plug 'https://github.com/MaxMEllon/vim-jsx-pretty'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }

call plug#end()

filetype plugin indent on

" Coc
let g:coc_global_extensions = [ 'coc-tsserver' ]
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gr <Plug>(coc-refactor)
nmap <silent> gs :call CocAction("doHover")<CR>

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" map tab to completion
inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" :  <SID>check_back_space() ? "\<TAB>" : coc#refresh()



" AirLine
let g:airline#extensions#tabline#enabled = 1
set laststatus=2
let g:airline_powerline_fonts=1

" Javascript
let javascript_enable_domhtmlcss=1
let b:javascript_fold=0
let javascript_ignore_javaScriptdoc=1

" DelimitMate
let delimitMate_expand_space=2
let delimitMate_expand_cr=2
let delimitMate_smart_matchpairs=1

" JSX
let g:jsx_ext_required = 1

" CTRL-P
let g:ctrlp_custom_ignore = {
      \ 'dir':  '\v[\/](node_modules|\.git)$',
      \ 'file': '\v\.(exe|so|dll)$',
      \ }
let g:ctrlp_working_path_mode = ''

" Lua
let lua_complete_dynamic = 0
" let includeexpr = 1
" let include = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" COLOR
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has("gui_running")
  set t_Co=256 " 256 colors
  let base16colorspace=256
  set background=dark
  colorscheme base16-solarized-light
  set guifont=SourceCodeProForPowerline-Regular:h19
  set cursorline cursorcolumn
  set guioptions-=m
  set guioptions-=T
  set guioptions-=r
  set guioptions-=L
endif

