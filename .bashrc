# Prevent "stdin: is not a tty" errors
if [[ $- != *i* ]]; then
  return 
fi

source "$HOME/.cargo/env"
source "$HOME/.git_completion.bash"
source "$HOME/.git_prompt.sh"

RESET="\[\017\]"
NORMAL="\[\033[0m\]"
REDBOLD="\[\033[1;31m\]"
CYAN="\[\033[1;36m\]"
export PS1="$RESET\w$CYAN \`__git_ps1 "%s"\`\n$REDBOLD$ $NORMAL"

stty werase undef
bind '"\C-w": backward-kill-word'

eval $(fnm env --multi)
export PATH=$PATH:~mike/.bin
alias ls="ls -G"
shopt -s checkwinsize
EDITOR=vim

alias ll="ls -l"

if test -f "/Users/mike/.bashrc_rapidapi"; then
  source /Users/mike/.bashrc_rapidapi
fi

HISTSIZE=100000
HISTCONTROL=ignoreboth # ignoredupes and ignorespace which ignores lines that begin with a space

# git completions for my aliases
__git_complete co git_checkout
__git_complete d git_diff
__git_complete dc git_diff
__git_complete s git_status
__git_complete p git_push
__git_complete cm git_commit

